# Assignment3

Assignment 3. PokeDex created using Angular

## Install

```
git clone https://gitlab.com/gitsetup/ng-pokemon
cd ng-pokemon
npm install
```

## Usage
When the user enters the site, they are prompted with an inputfield, where they can enter there name. 
if they have already visited the site and entered there name once before, 
they will be logged in to there existing account.
Once the user is logged in, 
They can choose to select pokemon from pokemon-catalouge and att to their Pokedex Trainer Page. 
They can also choose do delete each pokemon from their Pokedex Tranier Page.  


**API:**
- Create an enviroment file in the root-directory (Same level as the README.md file and package.json) and add the keys and URLs.
- Exchange "API-key" with the API-key.
- Exchange "Base-url" with the API URL.
- Exchange "Trainer-API" with the URL for the trainer API.
- Exchange "Pokemon-Base-URL" with the base URL for the Pokemon.
- Exchange "Img-URL" with the URL for the Pokemon images.

```
  production: false,
  apiBaseUrl: "<Base-URL>",
  apiTrainers: "<Trainer-API>",
  apiKey: "<API-Key>",
  apiBaseUrlPokemon: "<Pokemon-Base-URL>",
  apiImg: "<Img-URL>"
```
```
npm start
```

# Development
This application is created with Angular.
We are using tailwindcss as our css framework to speed up the design process.
We are fetching and posting data to an API to be able to save data.
When page is reloaded, we are using sessionStorage. 
We have also used figma to create a component tree. 

OBS! The apikey has been replaced! 
The previous commits have contains a apikey, so we have replaced it. 

## Contributors

This assignment has been done by [Elmin Nurkic](@gfunkmaster) and [Mattias Eriksson](@MattiasLeifEriksson)

### Uploaded to Heroku
- https://frozen-beach-09472.herokuapp.com/login


