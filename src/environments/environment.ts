export const environment = {
  production: false,
  apiBaseUrl: "https://en-noroff-api-new.herokuapp.com",
  apiTrainers: "https://en-noroff-api-new.herokuapp.com/trainers",
  apiKey: "",
  apiBaseUrlPokemon: "https://pokeapi.co/api/v2/pokemon?limit=50&offset=0",
  apiImg: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"
};


