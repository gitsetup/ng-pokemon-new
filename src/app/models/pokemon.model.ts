export interface SinglePokemon {
  image: string;
  name: string;
  url: string;
}

export interface PokemonResponse {
  count: number;
  next: string;
  previous: string;
  results: SinglePokemon[];
}
