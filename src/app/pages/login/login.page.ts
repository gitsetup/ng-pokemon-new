import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css'],
})
export class LoginPage {
  /*The Login page
This typescript file redirects the user to the Pokemon-catalogue when the user submits the login-form */
  constructor(private readonly router: Router) {}

  handleLogin(): void {
    this.router.navigateByUrl('/pokemon');
  }
}
