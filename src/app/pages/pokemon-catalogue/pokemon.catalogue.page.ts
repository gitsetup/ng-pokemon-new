import { Component, OnInit } from '@angular/core';
import { PokemonCatalogueService } from './../../services/pokemon-catalogue.service';
import { SinglePokemon } from './../../models/pokemon.model';

@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon.catalogue.page.html',
  styleUrls: ['./pokemon-catalogue.page.css']
})
export class PokemonCataloguePage implements OnInit {

  get pokemon(): SinglePokemon[] {
    return this.pokemonCatalougueService.pokemon!;
  }

  get loading(): boolean {
    return this.pokemonCatalougueService.loading
  }

   get error(): string {
    return this.pokemonCatalougueService.error;
  }

  constructor(private readonly pokemonCatalougueService: PokemonCatalogueService) {}

  ngOnInit(): void {
  this.pokemonCatalougueService.findAllPokemons();

  }

}
