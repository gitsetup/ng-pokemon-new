import { Component, OnInit } from '@angular/core';

import { TrainerService } from 'src/app/services/trainer.service';
import { Trainer } from 'src/app/models/trainer.model';
import { PokemonCatalogueService } from './../../services/pokemon-catalogue.service';
import { SinglePokemon } from './../../models/pokemon.model';

@Component({
	selector: 'app-profile',
	templateUrl: './profile.page.html',
	styleUrls: ['./profile.page.css'],
})
export class ProfilePage implements OnInit {
	get trainer(): Trainer | undefined {
		return this.trainerService.trainer;
	}

	constructor(
		private readonly trainerService: TrainerService,
		private readonly pokemonCatalogueService: PokemonCatalogueService,
	) {}

	ngOnInit(): void {
		//render the already pokemons 
		this.pokemonCatalogueService.findAllPokemons();
		//this.trainerService.inPokeDex(this.trainer?.pokemon)
	}

	
	//to get the image and show in profile site, so we are reading 
  get addPokemonImg(): SinglePokemon[] | []{
	//checking for that there is trainer and pokemons array is not 0 
    if (this.trainerService.trainer && this.trainerService.trainer.pokemon.length > 0) {
	// mapping through and finding the pokemon name to send
      const pokemonWithImage = this.trainerService.trainer.pokemon.map<any>(isPokemon => {
        return this.pokemonCatalogueService.pokemon?.find(inPokeDex => inPokeDex.name === isPokemon)
      })
      return pokemonWithImage
    }
    
    return [];
  }


	
}
