import { Injectable } from '@angular/core';
import {
	HttpClient,
	HttpErrorResponse,
	HttpResponse,
} from '@angular/common/http';
import { environment } from './../../environments/environment';
import { finalize, map, pipe, tap } from 'rxjs';
import { StorageKeys } from '../enums/storage-keys.enum';
import { PokemonResponse, SinglePokemon } from './../models/pokemon.model';
import { StorageUtil } from '../utils/storage.utils';

const { apiBaseUrlPokemon, apiImg } = environment;
@Injectable({
	providedIn: 'root',
})
export class PokemonCatalogueService {
	private _pokemon?: SinglePokemon[];

	private _error: string = '';
	private _loading: boolean = false;

	get pokemon(): SinglePokemon[] | undefined {
		return this._pokemon;
	}

	get error(): string {
		return this._error;
	}

	get loading(): boolean {
		return this._loading;
	}

	set pokemon(pokemon: SinglePokemon[] | undefined) {
		StorageUtil.storageSave<SinglePokemon[]>(StorageKeys.Pokemon, pokemon!);
		this._pokemon = pokemon;
	}

	constructor(private readonly http: HttpClient) {
		this._pokemon = StorageUtil.storageRead<SinglePokemon[]>(
			StorageKeys.Pokemon,
		);
	}


//function to find all pokemon
	public findAllPokemons(): void {
		//if there is no pokemons
		if (this.pokemon && (this._pokemon!.length > 0 || this.loading)) {
			return;
		}
		this._loading = true;
		this.http
			.get<PokemonResponse>(apiBaseUrlPokemon)
			.pipe(
				finalize(() => {
					this._loading = false;
				}),
			)
			.subscribe({
				next: (resp: PokemonResponse) => {
					//looping throgh the resp.result and adding the missing img
					this._pokemon = resp.results.map((pokemon, index) => {
						//adding a id on pokemon
						const pokemonId = index + 1;
						return {
							...pokemon,
							//taking the img api and in template string createing a new string with functiong url
							image: pokemon.image = `${apiImg}${pokemonId}.png`,
						};
					});
				},
				error: (error: HttpErrorResponse) => {
					console.log(error.message);
				},
			});
	}
}
