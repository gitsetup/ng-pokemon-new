import { Component, Input, OnInit } from '@angular/core';
import { TrainerService } from 'src/app/services/trainer.service';
import { SinglePokemon } from './../../models/pokemon.model';

@Component({
  selector: 'app-profile-list',
  templateUrl: './profile-list.component.html',
  styleUrls: ['./profile-list.component.css']
})
export class ProfileListComponent implements OnInit {


public isPokemonTaken: boolean = false;
@Input() pokemon:SinglePokemon[] | undefined = [];



  constructor(private trainerService: TrainerService) { }

  ngOnInit(): void {}

}
