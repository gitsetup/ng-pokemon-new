import { Component, Input, OnInit } from '@angular/core';

import { TrainerService } from 'src/app/services/trainer.service';
import { SinglePokemon } from './../../models/pokemon.model';
 import { ActivatedRoute } from '@angular/router';
import { PokemonCatalogueService } from './../../services/pokemon-catalogue.service';
@Component({
  selector: 'app-pokemon-catalogue-list',
  templateUrl: './pokemon-catalogue-list.component.html',
  styleUrls: ['./pokemon-catalogue-list.component.css']
})
export class PokemonCatalogueListComponent implements OnInit {

public isPokemonTaken: boolean = false;
@Input() pokemon: SinglePokemon[] = [];

  constructor(private readonly triainerService:TrainerService, 
              private readonly  pokemonCatalogueService:PokemonCatalogueService
  ) { }



  ngOnInit(): void {
        // finds all pokemon from start
       this.pokemonCatalogueService.findAllPokemons();
    
   }

}
