import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./guards/auth.guard";
import { PokemonCataloguePage } from "./pages/pokemon-catalogue/pokemon.catalogue.page"; 
import { LoginPage } from "./pages/login/login.page";
import { ProfilePage } from "./pages/profile/profile.page";

/*Handles the navigation between pages. Utilizes a authentication guard to prevent users from going to Profile and pokemon page
before they log in. */
const routes: Routes = [
{
    path: "",
    pathMatch: "full",
    redirectTo: "/login"
},
{
    path: "login",
    component: LoginPage,
    
    
},
{
    path: "pokemon",
    component: PokemonCataloguePage,
    canActivate: [AuthGuard]
},
{
    path: "profile",
    component: ProfilePage,
    canActivate: [AuthGuard]
}

]

@NgModule({
    imports: [
        RouterModule.forRoot(routes),

    ],
    exports: [
        RouterModule,
    ]
})
export class AppRoutingModule{

}